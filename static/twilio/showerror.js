'use strict';

import getUserFriendlyError from './userfriendlyerror.js';

export default function showError($modal, error) {
  $('div.alert', $modal).html(getUserFriendlyError(error));
  $modal.modal({
    backdrop: 'static',
    focus: true,
    keyboard: false,
    show: true
  });

  $('#show-error-label', $modal).text(`${error.name}${error.message
    ? `: ${error.message}`
    : ''}`);
}