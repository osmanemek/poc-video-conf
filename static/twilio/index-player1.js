'use strict';

import { isMobile } from './browser.js';
import joinRoom from './joinroom-player1.js';
import micLevel from './miclevel.js';
import selectMedia from './selectmedia.js';
import showError from './showerror.js';
import muteOrUnmuteYourMedia from './muteaudiovideo.js'

const $modals = $('#modals');
const $selectMicModal = $('#select-mic', $modals);
const $selectCameraModal = $('#select-camera', $modals);
const $showErrorModal = $('#show-error', $modals);
const $joinRoomModal = $('#join-room', $modals);
const $InvitePeopelModal = $('#invite-person', $modals);

const localScreenShare = document.getElementById("screenshare");
let screenShareStream;
let screenShareStreamTrack;
let screenShareLocalVideoStreamTrack;

let connectOptions = {
  audio: true,
  video: { height: 720, frameRate: 24, width: 1280 },
  bandwidthProfile: {
    video: {
      mode: 'presentation',
      maxTracks: 2,
      dominantSpeakerPriority: 'standard',
      renderDimensions: {
        high: {height:720, width:1280},
        standard: {height:480, width:854},
        low: {height:176, width:144}
      }
    }
  },
  trackSwitchOffMode: 'detected',
  dominantSpeaker: true,
  maxAudioBitrate: 16000, 
  preferredVideoCodecs: [{ codec: 'VP8', simulcast: true }],
  networkQuality: {local:0, remote: 0}
}

if (isMobile) {

  $('#btnsharescreen').hide();

  connectOptions = {
    audio: true,
    video: { height: 480, frameRate: 24, width: 640 },
    bandwidthProfile: {
      video: {
        mode: 'presentation',
        maxSubscriptionBitrate: 2500000,
        maxTracks: 2,
        dominantSpeakerPriority: 'standard',
        renderDimensions: {
          high: {height:1080, width:1920},
          standard: {height:720, width:1280},
          low: {height:176, width:144}
        }
      }
    },
    trackSwitchOffMode: 'detected',
    dominantSpeaker: true,
    maxAudioBitrate: 16000,
    preferredVideoCodecs: [{ codec: 'VP8', simulcast: true }],
    networkQuality: {local:0, remote: 0}
  }
}

const deviceIds = {
  audio: isMobile ? null : localStorage.getItem('audioDeviceId'),
  video: isMobile ? null : localStorage.getItem('videoDeviceId')
};

async function selectAndJoinRoom(error = null) {

  const identity = "player1";
  const roomName = "pocroom_UNGjBTU"

  try {
    const response = await fetch(`/createtoken/player1`);
    const token = await response.text();
    connectOptions.name = roomName;

    await joinRoom(token, connectOptions);
    
    return selectAndJoinRoom();
  } catch (error) {
    return selectAndJoinRoom(error);
  }
}

async function selectCamera() {
  if (deviceIds.video === null) {
    try {
      deviceIds.video = await selectMedia('video', $selectCameraModal, stream => {
        const $video = $('video', $selectCameraModal);
        $video.get(0).srcObject = stream;
      });
    } catch (error) {
      showError($showErrorModal, error);
      return;
    }
  }
  return selectAndJoinRoom();
}

async function selectMicrophone() {
  if (deviceIds.audio === null) {
    try {
      deviceIds.audio = await selectMedia('audio', $selectMicModal, stream => {
        const $levelIndicator = $('svg rect', $selectMicModal);
        const maxLevel = Number($levelIndicator.attr('height'));
        micLevel(stream, maxLevel, level => $levelIndicator.attr('y', maxLevel - level));
      });
    } catch (error) {
      //showError($showErrorModal, error);
      console.log(error);
      return;
    }
  }
  return selectCamera();
}

const changeCameraStatus = (event) => {
    var camstatus = $('#btncameraimg').attr('src');
    if(camstatus.indexOf("off")>0){
      muteOrUnmuteYourMedia(room,'video','unmute');
      $('#btncameraimg').attr('src','/static/icons/video.svg')
    }else{
      muteOrUnmuteYourMedia(room,'video','mute');
      $('#btncameraimg').attr('src','/static/icons/video-off.svg')

    }
    
};

const changeMicrophoneStatus = (event) => {
  var micstatus = $('#btnmicrophoneimg').attr('src');
  if(micstatus.indexOf("off")>0){
    muteOrUnmuteYourMedia(room,'audio','unmute');
    $('#btnmicrophoneimg').attr('src','/static/icons/mic.svg')
  }else{
    muteOrUnmuteYourMedia(room,'audio','mute');
    $('#btnmicrophoneimg').attr('src','/static/icons/mic-off.svg')
  }
};

const disconnectConference = (event) => {
  room.localParticipant.tracks.forEach((publication) => {
    const track = publication.track;
    track.stop();
    const elements = track.detach();
    elements.forEach((element) => element.remove());
  });
  room.disconnect();
};

const showLocalCam = (evet) => {
    const $room = $('#room');
    const $localparticipants = $('div#localparticipants', $room);
    $localparticipants.show();
}

const shareScreen = async (event) => {
  var sscreenstatus = $('#btnsharescreenimg').attr('src');
  if(sscreenstatus.indexOf("off")>0){
    try {
      localScreenShare.srcObject = null;
    } catch (error) {
      localScreenShare.src = null;
    }    

    room.localParticipant.unpublishTrack(screenShareLocalVideoStreamTrack);
    screenShareLocalVideoStreamTrack = null;
    screenShareStream  = null;
    screenShareStreamTrack = null;

    localScreenShare.style.display = "none";

    $('#btnsharescreenimg').attr('src','/static/icons/screen-share.svg')
  }else{
    if (typeof navigator === 'undefined' || !navigator.mediaDevices || !navigator.mediaDevices.getDisplayMedia) {
      console.log("getDisplayMedia is not supported")
    }else{
      let displayMediaOptions = {
        video: {
          height: 720,
          width: 1280,      
          cursor: "always"
        },
        audio: false
      };
    
      screenShareStream = await navigator.mediaDevices.getDisplayMedia(displayMediaOptions);
      screenShareStreamTrack = screenShareStream.getTracks()[0];
  
      localScreenShare.style.display = "block";
    
      try {
          localScreenShare.srcObject = screenShareStream;
      } catch (error) {
          localScreenShare.src = screenShareStream;
      }
      $('#btnsharescreenimg').attr('src','/static/icons/screen-share-off.svg');

      screenShareLocalVideoStreamTrack = await Twilio.Video.LocalVideoTrack(screenShareStreamTrack,{name:"screensharetrack"});

      //screenShareLocalVideoStreamTrack.mediaStreamTrack.onended = () => { shareScreenHandler() };

      await room.localParticipant.publishTrack(screenShareLocalVideoStreamTrack);
      return;
    }  
  }
}

window.addEventListener("load", async (event) => {
    await selectMicrophone();
});

//const btnCamera = document.getElementById("btncamera");
//btnCamera.addEventListener("click", changeCameraStatus);

const btnMicrophone = document.getElementById("btnmicrophone");
btnMicrophone.addEventListener("click", changeMicrophoneStatus);

const btnDisconnect = document.getElementById("btndisconnect");
btnDisconnect.addEventListener("click", disconnectConference);

/*
const btnsharescreenimg = document.getElementById("btnsharescreen");
btnsharescreenimg.addEventListener("click", async (event) => {
  await shareScreen();
});
*/

const btnlocalcam = document.getElementById("btnlocalcam");
btnlocalcam.addEventListener("click", showLocalCam);
