'use strict';

import { isMobile } from './browser.js';

const $leave = $('#leave-room');
const $room = $('#room');
const $localparticipants = $('div#localparticipants', $room);
const $remoteparticipants = $('div#remoteparticipants', $room);
const $refereeparticipants = $('div#refereeparticipants', $room);
const localScreenShare = document.getElementById("screenshare");

function setupParticipantContainer(participant, room, participantID) {
  const { identity, sid } = participant;

  let cssname = ""
  if(participantID=="player2"){
    cssname = "localparticipant"
  } else if(participantID=="player1"){
    cssname = "remoteparticipant"
  } else if(participantID=="referee"){
    cssname = "refereeparticipant"
  }

  const $container = $(`<div class="${cssname} col-lg-3 col-xl-3" data-identity="${identity}" id="${sid}">
    <audio autoplay ${participant === room.localParticipant ? 'muted' : ''} style="opacity: 0"></audio>
    <video autoplay muted playsinline style="opacity: 0" id="vid${sid}" class="locvid"></video>
  </div>`);

  if(participantID=="player2"){
    $localparticipants.append($container);
  } else if(participantID=="player1"){
    $remoteparticipants.append($container);
  } else if(participantID=="referee"){
    $refereeparticipants.append($container);
  }

    //localvideo hide
    $localparticipants.hide();
}

function setVideoPriority(participant, priority) {
  participant.videoTracks.forEach(publication => {
    const track = publication.track;
    if (track && track.setPriority) {
      track.setPriority(priority);
    }
  });
}

function attachTrack(track, participant) {
  if(track.name=="screensharetrack"){
    // Attach the Participant's Share Screen Track
    localScreenShare.style.display = "block";
    track.attach(localScreenShare);
    if(participant === room.localParticipant){
      $('#btnsharescreenimg').attr('src','/static/icons/screen-share-off.svg');
    }else{
      $('#btnsharescreen').attr('style','display:none');
    }

  }else{
    let $media;

  if(participant.identity=="player2"){
    $media = $(`div#${participant.sid} > ${track.kind}`, $localparticipants);
  } else if(participant.identity=="player1"){
    $media = $(`div#${participant.sid} > ${track.kind}`, $remoteparticipants);
  } else if(participant.identity=="referee"){
    $media = $(`div#${participant.sid} > ${track.kind}`, $refereeparticipants);
  }

    $media.css('opacity', '');
    track.attach($media.get(0));
  }
}

function detachTrack(track, participant) {
  if(track.name=="screensharetrack"){
    // Attach the Participant's Share Screen Track
    localScreenShare.style.display = "none";
    track.detach(localScreenShare);
    $('#btnsharescreenimg').attr('src','/static/icons/screen-share.svg');
    $('#btnsharescreen').attr('style','display:block;margin-left:5px;');
  }else{
    let $media;

  if(participant.identity=="player2"){
    $media = $(`div#${participant.sid} > ${track.kind}`, $localparticipants);
  } else if(participant.identity=="player1"){
    $media = $(`div#${participant.sid} > ${track.kind}`, $remoteparticipants);
  } else if(participant.identity=="referee"){
    $media = $(`div#${participant.sid} > ${track.kind}`, $refereeparticipants);
  }

    $media.css('opacity', '0');
    track.detach($media.get(0));
  }
}

function participantConnected(participant, room) {
  setupParticipantContainer(participant, room, participant.identity);

  participant.tracks.forEach(publication => {
    trackPublished(publication, participant);
  });

  participant.on('trackPublished', publication => {
    trackPublished(publication, participant);
  });
}

function participantDisconnected(participant, room) {
  if(participant.identity=="player2"){
    $(`div#${participant.sid}`, $localparticipants).remove();
  } else if(participant.identity=="player1"){
    $(`div#${participant.sid}`, $remoteparticipants).remove();
  } else if(participant.identity=="referee"){
    $(`div#${participant.sid}`, $refereeparticipants).remove();
  }
}

function trackPublished(publication, participant) {
  if (publication.track) {
    attachTrack(publication.track, participant);
  }

  publication.on('subscribed', track => {
    attachTrack(track, participant);
  });

  publication.on('unsubscribed', track => {
    detachTrack(track, participant);
  });
}

export default async function joinRoom(token, connectOptions) {
  const room = await Twilio.Video.connect(token, connectOptions);

  let localVideoTrack = Array.from(room.localParticipant.videoTracks.values())[0].track;

  window.room = room;

  participantConnected(room.localParticipant, room);

  room.participants.forEach(participant => {
    participantConnected(participant, room);
  });

  room.on('participantConnected', participant => {
    participantConnected(participant, room);
  });

  room.on('participantDisconnected', participant => {
    participantDisconnected(participant, room);
  });

  $('#buttonsbar').show();

  $leave.click(function onLeave() {
    $leave.off('click', onLeave);
    room.disconnect();
  });

  return new Promise((resolve, reject) => {
    window.onbeforeunload = () => {
      room.disconnect();
    };

    if (isMobile) {
      window.onpagehide = () => {
        room.disconnect();
      };

      document.onvisibilitychange = async () => {
        if (document.visibilityState === 'hidden') {
          localVideoTrack.stop();
          room.localParticipant.unpublishTrack(localVideoTrack);
        } else {
          localVideoTrack = await Twilio.Video.createLocalVideoTrack(connectOptions.video);
          await room.localParticipant.publishTrack(localVideoTrack);
        }
      };
    }

    room.once('disconnected', (room, error) => {
      window.onbeforeunload = null;
      if (isMobile) {
        window.onpagehide = null;
        document.onvisibilitychange = null;
      }

      localVideoTrack.stop();

      participantDisconnected(room.localParticipant, room);

      room.participants.forEach(participant => {
        participantDisconnected(participant, room);
      });

      window.room = null;

      if (error) {    
        reject(error);
      } else {
        resolve();
      }
    });
  });
}
