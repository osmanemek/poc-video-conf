from fastapi import FastAPI,Request
from twilio.rest import Client
from fastapi.responses import HTMLResponse
from fastapi.staticfiles import StaticFiles
from fastapi.templating import Jinja2Templates
import random
import string

app = FastAPI()
app.mount("/static", StaticFiles(directory="static"), name="static")
templates = Jinja2Templates(directory="templates")

def get_random_string(length):
    letters = string.ascii_letters + string.digits
    result_str = ''.join(random.choice(letters) for i in range(length))
    return result_str

@app.get("/player1", response_class=HTMLResponse)
async def player1(request: Request,):
    return templates.TemplateResponse("player1.html", {"request": request})

@app.get("/player2", response_class=HTMLResponse)
async def player1(request: Request,):
    return templates.TemplateResponse("player2.html", {"request": request})

@app.get("/referee", response_class=HTMLResponse)
async def player1(request: Request,):
    return templates.TemplateResponse("referee.html", {"request": request})

@app.get("/roomcreate")
async def roomcreate():
    account_sid = "AC6be39XXXXXXXXXXXXXX"
    auth_token = "73f89aXXXXXXXXXXXXXXXXXXX"

    roomName = "pocroom_%s" % get_random_string(7)
    roomType = "group-small"
    max_participants = 3

    client = Client(account_sid, auth_token)
    room = client.video.rooms.create(unique_name=roomName,type=roomType,max_participants=max_participants)

    return {"room_sid": room.sid,"room_name":roomName}

@app.get("/createtoken/{identity}", response_class=HTMLResponse)
def createtoken(identity: str):
    from twilio.jwt.access_token.grants import VideoGrant
    from twilio.jwt.access_token import AccessToken

    active_room_uuid = "pocroom_UNGjBTU"

    # twilio api keys
    TWILIO_ACCOUNT_SID = 'AC6be39XXXXXXXXXXXXXXXXX'
    TWILIO_API_KEY = 'SK7XXXXXXXXXXXXXXXXXXXXXX'
    TWILIO_SECRET_KEY = 'GLkXXXXXXXXXXXXXXXXXXXXXX'

    token = AccessToken(TWILIO_ACCOUNT_SID, TWILIO_API_KEY, TWILIO_SECRET_KEY, identity=identity)

    video_grant = VideoGrant(room=active_room_uuid)
    token.add_grant(video_grant)

    return str(token.to_jwt().decode('utf-8'))